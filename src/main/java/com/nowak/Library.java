package com.nowak;

import com.nowak.data.BookStorage;
import com.nowak.data.LendStorage;
import com.nowak.model.Book;

import java.util.List;
import java.util.Map;

public class Library {

    private BookStorage bookStorage;
    private LendStorage lendStorage;
    private static Long generatedId = 0L;

    private static Long generateId() {
        return generatedId++;
    }

    public Library(BookStorage bookStorage, LendStorage lendStorage) {
        this.bookStorage = bookStorage;
        this.lendStorage = lendStorage;
    }

    public Boolean addBook(String title, String author, String year) {
            return bookStorage.addBook(new Book(title, author, year, generateId()));
    }

    public void listAllBooks() {
        Map<Book, Map<Boolean, Long>> books = bookStorage.getBooksAndLendStatus();

        for (Map.Entry<Book, Map<Boolean, Long>> entry : books.entrySet()) {

            Book book = entry.getKey();
            Long availableCount = entry.getValue()
                    .get(false) !=null ? entry.getValue().get(false) : 0L;
            Long lentCount = entry.getValue()
                    .get(true) !=null ? entry.getValue().get(true) : 0L;

            System.out.println("{" + book.getTitle() + ", " +
                    book.getAuthor() + ", " +
                    book.getYear() + " }\nLent copies: " +
                    lentCount + "\nAvailable copies: " +
                    availableCount + "\n");
        }
    }


    public boolean removeBook(Long id) {
       return bookStorage.removeBook(id);
    }

    public Book getBook(Long id)
    {
        return bookStorage.getBook(id);
    }

    public List<Book> searchBooks(String title, String author, String year)
    {
        return bookStorage.search(title, author, year);
    }

    public boolean lendBook(Long id, String personName)
    {
        Book book =  bookStorage.getBook(id);
        if(!book.getLend() && lendStorage.addLend(id, personName)) {
            book.setLend(true);
            return true;
        }
        return false;
    }

    public String getBookInfo(Long id) {
        Book book = bookStorage.getBook(id);

        return book + "status: " + ((book.getLend())
                        ? "Book lent by " + lendStorage.getPerson(id)
                        : "Available");
    }
}
