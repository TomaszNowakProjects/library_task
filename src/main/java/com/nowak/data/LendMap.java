package com.nowak.data;

import java.util.HashMap;
import java.util.Map;

public class LendMap implements LendStorage {

    private Map<Long, String> lendMap = new HashMap<>();


    public Boolean addLend(Long bookId, String personName) {

        boolean canAdd = bookId !=null && personName != null;
        if(canAdd) {
            lendMap.put(bookId, personName);
        }
        return canAdd;
    }

    public String getPerson(Long bookId) {
        return lendMap.get(bookId);
    }

    public int getSize() {
        return lendMap.size();
    }
}
