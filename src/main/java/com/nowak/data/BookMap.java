package com.nowak.data;

import com.nowak.model.Book;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class BookMap  implements BookStorage {

    private Map<Long, Book> bookMap = new HashMap<>();

    public Boolean addBook(Book book)
    {
        boolean canAdd = book != null;
        if(canAdd) {
            bookMap.put(book.getId(),book);
        }
        return canAdd;
    }

    public Boolean removeBook(Long id)
    {
        if(bookMap.containsKey(id)) {
            Book book = bookMap.get(id);
            if(!book.getLend()) {
                bookMap.remove(id);
                return true;
            }
        }
        return false;
    }

    public Book getBook(Long id) {
        return bookMap.get(id);
    }

    public List<Book> getAllBooks()
    {
        return new ArrayList<>(bookMap.values());
    }

    public List<Book> search(String title, String author, String year)
    {
       return bookMap.values().stream()
               .filter(e-> (e.getTitle().equals(title) || title == null) &&
                       (e.getAuthor().equals(author) || author == null) &&
                       (e.getYear().equals(year) || year ==null))
               .collect(Collectors.toList());
    }

   public Map<Book, Map<Boolean, Long>> getBooksAndLendStatus() {

        return bookMap.values().stream()
                .collect(groupingBy(book -> book,
                        groupingBy(Book::getLend, counting())));
    }

    public int getSize() {
        return bookMap.size();
    }
}


