package com.nowak.data;

public interface LendStorage {
     Boolean addLend(Long bookId, String personName);
     String getPerson(Long bookId);
}
