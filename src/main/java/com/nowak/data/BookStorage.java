package com.nowak.data;

import com.nowak.model.Book;

import java.util.List;
import java.util.Map;

public interface BookStorage {

    Boolean addBook(Book book);
    Boolean removeBook(Long id);
    Book getBook(Long id);
    List<Book> getAllBooks();
    List<Book> search(String title, String author, String year);
    Map<Book, Map<Boolean, Long>> getBooksAndLendStatus();

}
