package com.nowak.model;

import java.util.Objects;

public class Book {

    private Long id;
    private String title;
    private String author;
    private String year;
    private Boolean isLend;

    public Book(String title, String author, String year, Long id) {
        this.title = title;
        this.author = author;
        this.year = year;
        this.id = id;
        isLend = false;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getYear() {
        return year;
    }

    public Long getId() {
        return id;
    }

    public Boolean getLend() {
        return isLend;
    }

    public void setLend(Boolean lend) {
        isLend = lend;
    }

    @Override
    public String toString() {
        return "id: " + id + "\n" +
                "title: " + title +"\n" +
                "author: " + author + "\n" +
                "year: " + year + "\n";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Book book = (Book) obj;
        if (!Objects.equals(this.title, book.title)) {
            return false;
        }
        if (!Objects.equals(this.author, book.author)) {
            return false;
        }
        if (!Objects.equals(this.year, book.year)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int titleHash = title != null ? title.hashCode() : 0;
        int authorHash = author != null ? author.hashCode() : 0;
        int yearHash = year != null ? year.hashCode() : 0;
        return titleHash + authorHash + yearHash;
    }

}
