package com.nowak.data;

import org.junit.Test;

import static org.junit.Assert.*;

public class LendMapTest {

    @Test
    public void addLendTestTrue() {
        LendMap testLendMap = new LendMap();
        assertTrue(testLendMap.addLend(0L, "TestPerson"));
        assertEquals(1, testLendMap.getSize());
    }

    @Test
    public void addLendTestFalseNull() {
        LendMap testLendMap = new LendMap();

        assertFalse(testLendMap.addLend(null, null));
        assertEquals(0, testLendMap.getSize());
    }

    @Test
    public void addLendTestFalseNullId() {
        LendMap testLendMap = new LendMap();

        assertFalse(testLendMap.addLend(null, "TestPerson"));
        assertEquals(0, testLendMap.getSize());
    }

    @Test
    public void addLendTestFalseNullName() {
        LendMap testLendMap = new LendMap();

        assertFalse(testLendMap.addLend(0L, null));
        assertEquals(0, testLendMap.getSize());
    }

    @Test
    public void getPersonTestPersonInMap() {
        LendMap testLendMap = new LendMap();
        assertTrue(testLendMap.addLend(0L, "TestPerson"));
        assertEquals("TestPerson", testLendMap.getPerson(0L));
    }

    @Test
    public void getPersonTestPersonNotInMap() {
        LendMap testLendMap = new LendMap();
        assertTrue(testLendMap.addLend(0L, "TestPerson"));
        assertNull(testLendMap.getPerson(3L));
    }

    @Test
    public void getSizeTest() {
        LendMap testLendMap = new LendMap();
        assertTrue(testLendMap.addLend(0L, "TestPerson"));
        assertTrue(testLendMap.addLend(1L, "TestPerson"));
        assertTrue(testLendMap.addLend(2L, "TestPerson"));

        assertEquals(3, testLendMap.getSize());

    }
}