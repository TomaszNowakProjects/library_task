package com.nowak.data;

import com.nowak.model.Book;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class BookMapTest {

    @Test
    public void addBookTestTrue() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);

        assertTrue(testBookMap.addBook(testBook));
        assertEquals(1, testBookMap.getSize());
    }

    @Test
    public void addBookTestFalse() {
        BookMap testBookMap = new BookMap();

        assertFalse(testBookMap.addBook(null));
        assertEquals(0, testBookMap.getSize());
    }

    @Test
    public void addBookTestSameIdBooks() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        Book testBook2 =new Book("testTitle2","TestAuthor", "2019",0L);

        assertTrue(testBookMap.addBook(testBook));
        assertTrue(testBookMap.addBook(testBook2));
        assertEquals(testBook2, testBookMap.getBook(0L));
        assertEquals(1, testBookMap.getSize());
    }

    @Test
    public void removeBookTestTrue() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        Book testBook2 =new Book("testTitle2","TestAuthor", "2019",1L);
        testBookMap.addBook(testBook);
        testBookMap.addBook(testBook2);

        assertTrue(testBookMap.removeBook(1L));
        assertEquals(1,testBookMap.getSize());
    }

    @Test
    public void removeBookTestBookNotInMap() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        Book testBook2 =new Book("testTitle2","TestAuthor", "2019",1L);
        testBookMap.addBook(testBook);
        testBookMap.addBook(testBook2);

        assertFalse(testBookMap.removeBook(5L));
        assertEquals(2,testBookMap.getSize());
    }

    @Test
    public void removeBookTestBookLent() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        Book testBook2 =new Book("testTitle2","TestAuthor", "2019",1L);
        testBook.setLend(true);
        testBookMap.addBook(testBook);
        testBookMap.addBook(testBook2);

        assertFalse(testBookMap.removeBook(0L));
        assertEquals(2,testBookMap.getSize());
    }

    @Test
    public void getBookTestBookInMap() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        testBookMap.addBook(testBook);

        assertEquals(testBook, testBookMap.getBook(0L));
    }

    @Test
    public void getBookTestBookNotMap() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        testBookMap.addBook(testBook);

        assertNotEquals(testBook, testBookMap.getBook(99L));
    }

    @Test
    public void getAllBooksTest() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        Book testBook2 =new Book("testTitle2","TestAuthor", "2019",1L);
        Book testBook3 =new Book("testTitle3","TestAuthor", "2019",2L);
        testBookMap.addBook(testBook);
        testBookMap.addBook(testBook2);
        testBookMap.addBook(testBook3);
        ArrayList<Book> expectedList = new ArrayList<>();
        expectedList.add(testBook);
        expectedList.add(testBook2);
        expectedList.add(testBook3);

        assertArrayEquals(expectedList.toArray(),testBookMap.getAllBooks().toArray());
    }

    @Test
    public void getAllBooksTestEmptyList() {
        BookMap testBookMap = new BookMap();

        assertEquals(0, testBookMap.getSize());
        assertArrayEquals(new ArrayList<Book>().toArray(),testBookMap.getAllBooks().toArray());
    }

    @Test
    public void searchTestAllNull() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        Book testBook2 =new Book("testTitle","TestAuthor2", "2018",1L);
        Book testBook3 =new Book("testTitle3","TestAuthor2", "2020",2L);
        Book testBook4 =new Book("testTitle4","TestAuthor3", "2020",3L);
        testBookMap.addBook(testBook);
        testBookMap.addBook(testBook2);
        testBookMap.addBook(testBook3);
        testBookMap.addBook(testBook4);
        List<Book> result = testBookMap.search(null, null,null );

        assertEquals(4, result.size());
        assertEquals(testBook, result.get(0));
        assertEquals(testBook2, result.get(1));
        assertEquals(testBook3, result.get(2));
        assertEquals(testBook4, result.get(3));

    }

    @Test
    public void searchTestTitle() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        Book testBook2 =new Book("testTitle","TestAuthor2", "2018",1L);
        Book testBook3 =new Book("testTitle3","TestAuthor2", "2020",2L);
        Book testBook4 =new Book("testTitle4","TestAuthor3", "2020",3L);
        testBookMap.addBook(testBook);
        testBookMap.addBook(testBook2);
        testBookMap.addBook(testBook3);
        testBookMap.addBook(testBook4);
        List<Book> result = testBookMap.search("testTitle", null,null );

        assertEquals(2, result.size());
        assertEquals(testBook, result.get(0));
        assertEquals(testBook2, result.get(1));

    }

    @Test
    public void searchTestAuthor() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        Book testBook2 =new Book("testTitle","TestAuthor2", "2018",1L);
        Book testBook3 =new Book("testTitle3","TestAuthor2", "2020",2L);
        Book testBook4 =new Book("testTitle4","TestAuthor3", "2020",3L);
        testBookMap.addBook(testBook);
        testBookMap.addBook(testBook2);
        testBookMap.addBook(testBook3);
        testBookMap.addBook(testBook4);
        List<Book> result = testBookMap.search(null, "TestAuthor2",null );

        assertEquals(2, result.size());
        assertEquals(testBook2, result.get(0));
        assertEquals(testBook3, result.get(1));

    }

    @Test
    public void searchTestYear() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        Book testBook2 =new Book("testTitle","TestAuthor2", "2018",1L);
        Book testBook3 =new Book("testTitle3","TestAuthor2", "2020",2L);
        Book testBook4 =new Book("testTitle4","TestAuthor3", "2020",3L);
        testBookMap.addBook(testBook);
        testBookMap.addBook(testBook2);
        testBookMap.addBook(testBook3);
        testBookMap.addBook(testBook4);
        List<Book> result = testBookMap.search(null, null,"2020" );

        assertEquals(2, result.size());
        assertEquals(testBook3, result.get(0));
        assertEquals(testBook4, result.get(1));
    }

    @Test
    public void searchTestTitleAuthor() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        Book testBook2 =new Book("testTitle","TestAuthor2", "2018",1L);
        Book testBook3 =new Book("testTitle3","TestAuthor2", "2020",2L);
        Book testBook4 =new Book("testTitle4","TestAuthor3", "2020",3L);
        testBookMap.addBook(testBook);
        testBookMap.addBook(testBook2);
        testBookMap.addBook(testBook3);
        testBookMap.addBook(testBook4);
        List<Book> result = testBookMap.search("testTitle", "TestAuthor2",null );

        assertEquals(1, result.size());
        assertEquals(testBook2, result.get(0));
    }

    @Test
    public void searchTestTitleYear() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        Book testBook2 =new Book("testTitle","TestAuthor2", "2018",1L);
        Book testBook3 =new Book("testTitle3","TestAuthor2", "2020",2L);
        Book testBook4 =new Book("testTitle3","TestAuthor3", "2020",3L);
        testBookMap.addBook(testBook);
        testBookMap.addBook(testBook2);
        testBookMap.addBook(testBook3);
        testBookMap.addBook(testBook4);
        List<Book> result = testBookMap.search("testTitle3", null,"2020" );

        assertEquals(2, result.size());
        assertEquals(testBook3, result.get(0));
        assertEquals(testBook4, result.get(1));
    }

    @Test
    public void searchTestAuthorYear() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        Book testBook2 =new Book("testTitle","TestAuthor2", "2018",1L);
        Book testBook3 =new Book("testTitle3","TestAuthor2", "2018",2L);
        Book testBook4 =new Book("testTitle4","TestAuthor3", "2020",3L);
        testBookMap.addBook(testBook);
        testBookMap.addBook(testBook2);
        testBookMap.addBook(testBook3);
        testBookMap.addBook(testBook4);
        List<Book> result = testBookMap.search(null, "TestAuthor2","2018" );

        assertEquals(2, result.size());
        assertEquals(testBook2, result.get(0));
        assertEquals(testBook3, result.get(1));
    }

    @Test
    public void searchTestTitleAuthorYear() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        Book testBook2 =new Book("testTitle","TestAuthor2", "2018",1L);
        Book testBook3 =new Book("testTitle3","TestAuthor2", "2020",2L);
        Book testBook4 =new Book("testTitle4","TestAuthor3", "2020",3L);
        testBookMap.addBook(testBook);
        testBookMap.addBook(testBook2);
        testBookMap.addBook(testBook3);
        testBookMap.addBook(testBook4);
        List<Book> result = testBookMap.search("testTitle", "TestAuthor","2019" );

        assertEquals(1, result.size());
        assertEquals(testBook, result.get(0));
    }

    @Test
    public void getBooksAndLendStatus() {
        BookMap testBookMap = new BookMap();
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        Book testBook2 =new Book("testTitle","TestAuthor", "2019",1L);
        testBook2.setLend(true);
        Book testBook3 =new Book("testTitle","TestAuthor", "2019",2L);
        testBook3.setLend(true);
        Book testBook4 =new Book("testTitle2","TestAuthor2", "2020",3L);
        testBookMap.addBook(testBook);
        testBookMap.addBook(testBook2);
        testBookMap.addBook(testBook3);
        testBookMap.addBook(testBook4);

        Map<Book, Map<Boolean, Long>> result = testBookMap.getBooksAndLendStatus();
        assertEquals(2, result.size());
        assertEquals(2L, (long) result.get(testBook).get(true));
        assertEquals(1L, (long) result.get(testBook).get(false));
        assertEquals(1L, (long) result.get(testBook4).get(false));
        assertTrue(null == result.get(testBook4).get(true));

    }

    @Test
    public void getBooksAndLendStatusEmpty() {
        BookMap testBookMap = new BookMap();
        Map<Book, Map<Boolean, Long>> result = testBookMap.getBooksAndLendStatus();
        assertEquals(0, result.size());
    }
}