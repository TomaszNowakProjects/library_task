package com.nowak;

import com.nowak.data.BookMap;
import com.nowak.data.LendMap;
import com.nowak.model.Book;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LibraryTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Test
    public void addBookTestTrue() {
        BookMap bookMapMock = mock(BookMap.class);
        LendMap lendMapMock = mock(LendMap.class);
        Book testBook = new Book("testTitle","TestAuthor", "2019",0L);
        when(bookMapMock.addBook(testBook)).thenReturn(true);
        Library testLibrary= new Library(bookMapMock, lendMapMock);

        assertTrue(testLibrary.addBook("testTitle","TestAuthor","2019"));
    }

    @Test
    public void addBookTestFalse() {
        BookMap bookMapMock = mock(BookMap.class);
        LendMap lendMapMock = mock(LendMap.class);
        Book testBook = new Book("testTitle","TestAuthor", "2019",0L);
        when(bookMapMock.addBook(testBook)).thenReturn(false);
        Library testLibrary= new Library(bookMapMock, lendMapMock);

        assertFalse(testLibrary.addBook("testTitle","TestAuthor","2019"));
    }

    @Test
    public void listAllBooksTest() {

        System.setOut(new PrintStream(outContent));
        BookMap bookMapMock = mock(BookMap.class);
        LendMap lendMapMock = mock(LendMap.class);
        Book testBook = new Book("testTitle","TestAuthor", "2019",0L);
        Book testBook2 = new Book("testTitle2","TestAuthor2", "2019",1L);
        Map<Book, Map<Boolean, Long>> mockMap = new HashMap<>();
        Map<Boolean, Long> tempMap = new HashMap<>();
        tempMap.put(true, 3L);
        tempMap.put(false, 1L);
        mockMap.put(testBook,tempMap);
        tempMap = new HashMap<>();
        tempMap.put(true, null);
        tempMap.put(false, 9L);
        mockMap.put(testBook2,tempMap);
        when(bookMapMock.getBooksAndLendStatus()).thenReturn(mockMap);
        Library testLibrary= new Library(bookMapMock, lendMapMock);
        String expectedResult1 = "{" + "testTitle" + ", " +
                "TestAuthor" + ", " +
                "2019" + " }\nLent copies: " +
                3L + "\nAvailable copies: " +
                1L + "\n\n" +
                "{" + "testTitle2" + ", " +
                "TestAuthor2" + ", " +
                "2019" + " }\nLent copies: " +
                0L + "\nAvailable copies: " +
                9L + "\n\n";

        String expectedResult2 = "{" + "testTitle2" + ", " +
                "TestAuthor2" + ", " +
                "2019" + " }\nLent copies: " +
                0L + "\nAvailable copies: " +
                9L + "\n\n" +
                "{" + "testTitle" + ", " +
                "TestAuthor" + ", " +
                "2019" + " }\nLent copies: " +
                3L + "\nAvailable copies: " +
                1L + "\n\n";

        testLibrary.listAllBooks();
        assertTrue(expectedResult1.equals(outContent.toString())
                || expectedResult2.equals(outContent.toString()));
        System.setOut(originalOut);
    }

    @Test
    public void removeBookTestTrue() {
        BookMap bookMapMock = mock(BookMap.class);
        LendMap lendMapMock = mock(LendMap.class);
        when(bookMapMock.removeBook(1L)).thenReturn(true);
        Library testLibrary= new Library(bookMapMock, lendMapMock);

        assertTrue(testLibrary.removeBook(1L));
    }

    @Test
    public void removeBookTestFalse() {
        BookMap bookMapMock = mock(BookMap.class);
        LendMap lendMapMock = mock(LendMap.class);
        when(bookMapMock.removeBook(1L)).thenReturn(false);
        Library testLibrary= new Library(bookMapMock, lendMapMock);

        assertFalse(testLibrary.removeBook(1L));
    }

    @Test
    public void getBookTest() {
        BookMap bookMapMock = mock(BookMap.class);
        LendMap lendMapMock = mock(LendMap.class);
        Book testBook = new Book("testTitle","TestAuthor", "2019",0L);
        when(bookMapMock.getBook(0L)).thenReturn(testBook);
        Library testLibrary= new Library(bookMapMock, lendMapMock);

        assertEquals(testBook,testLibrary.getBook(0L ));
        assertEquals(testBook.getId(),testLibrary.getBook(0L ).getId());
    }

    @Test
    public void searchBooksTest() {
        BookMap bookMapMock = mock(BookMap.class);
        LendMap lendMapMock = mock(LendMap.class);
        List<Book> testList = new ArrayList<>();
        Book testBook1 =new Book("testTitle","TestAuthor", "2019",0L);
        Book testBook2 = new Book("testTitle","TestAuthor", "2000",1L);
        testList.add(testBook1);
        testList.add(testBook2);
        when(bookMapMock.search("testTitle","TestAuthor",null))
                .thenReturn(testList);
        Library testLibrary= new Library(bookMapMock, lendMapMock);

        List<Book> resultList = testLibrary
                .searchBooks("testTitle","TestAuthor",null);
        assertEquals(2, resultList.size());
        assertEquals(testBook1, resultList.get(0));
        assertEquals(testBook2, resultList.get(1));



    }

    @Test
    public void lendBookTestTrue() {
        BookMap bookMapMock = mock(BookMap.class);
        LendMap lendMapMock = mock(LendMap.class);
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        when(bookMapMock.getBook(0L)).thenReturn(testBook);
        when(lendMapMock.addLend(0L, "TestPerson")).thenReturn(true);
        Library testLibrary= new Library(bookMapMock, lendMapMock);

        assertTrue(testLibrary.lendBook(0L, "TestPerson"));


    }

    @Test
    public void lendBookTestFalseAlreadyLent() {
        BookMap bookMapMock = mock(BookMap.class);
        LendMap lendMapMock = mock(LendMap.class);
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        testBook.setLend(true);
        when(bookMapMock.getBook(0L)).thenReturn(testBook);
        when(lendMapMock.addLend(0L, "TestPerson")).thenReturn(true);
        Library testLibrary= new Library(bookMapMock, lendMapMock);

        assertFalse(testLibrary.lendBook(0L, "TestPerson"));
    }

    @Test
    public void lendBookTestFalseCanNotAddToList() {
        BookMap bookMapMock = mock(BookMap.class);
        LendMap lendMapMock = mock(LendMap.class);
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        when(bookMapMock.getBook(0L)).thenReturn(testBook);
        when(lendMapMock.addLend(0L, "TestPerson")).thenReturn(false);
        Library testLibrary= new Library(bookMapMock, lendMapMock);

        assertFalse(testLibrary.lendBook(0L, "TestPerson"));
    }

    @Test
    public void getBookInfoTestBookAvailable() {
        BookMap bookMapMock = mock(BookMap.class);
        LendMap lendMapMock = mock(LendMap.class);
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        when(bookMapMock.getBook(0L)).thenReturn(testBook);
        Library testLibrary= new Library(bookMapMock, lendMapMock);
        String expectedValue =testBook + "status: Available";

        assertEquals(expectedValue, testLibrary.getBookInfo(0L));
    }
    @Test
    public void getBookInfoTestBookLent() {
        BookMap bookMapMock = mock(BookMap.class);
        LendMap lendMapMock = mock(LendMap.class);
        Book testBook =new Book("testTitle","TestAuthor", "2019",0L);
        testBook.setLend(true);
        when(bookMapMock.getBook(0L)).thenReturn(testBook);
        when(lendMapMock.getPerson(0L)).thenReturn("TestPerson");
        Library testLibrary= new Library(bookMapMock, lendMapMock);
        String expectedValue =testBook + "status: Book lent by TestPerson";

        assertEquals(expectedValue, testLibrary.getBookInfo(0L));
    }
}