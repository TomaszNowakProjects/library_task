package com.nowak.model;


import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;


public class BookTest {


    @Test
    public void getTitleValueTest() {
        Book testBook = new Book("testTitle","TestAuthor", "2019",1L);
        assertEquals("testTitle", testBook.getTitle());
    }

    @Test
    public void getTitleTestNull() {
        Book testBook = new Book(null,null, null,null);
        assertNull(testBook.getTitle());
    }

    @Test
    public void getAuthorTest() {
        Book testBook = new Book("testTitle","TestAuthor", "2019",1L);
        assertEquals("TestAuthor", testBook.getAuthor());
    }

    @Test
    public void getAuthorTestNull() {
        Book testBook = new Book(null,null, null,null);
        assertNull(testBook.getAuthor());
    }

    @Test
    public void getYearTest() {
        Book testBook = new Book("testTitle","TestAuthor", "2019",1L);
        assertEquals("2019", testBook.getYear());
    }

    @Test
    public void getYearTestNull() {
        Book testBook = new Book(null,null, null,null);
        assertNull(testBook.getYear());
    }

    @Test
    public void getIdTest() {
        Book testBook = new Book("testTitle","TestAuthor", "2019",1L);
        assertEquals(1L, (long) testBook.getId());
    }

    @Test
    public void getIdTestNull() {
        Book testBook = new Book(null,null, null,null);
        assertEquals(null,  testBook.getId());
    }

    @Test
    public void getLendTestFalse() {
        Book testBook = new Book("testTitle","TestAuthor", "2019",1L);
        assertFalse(testBook.getLend());
    }

    @Test
    public void getSetLendTestTrue() {
        Book testBook = new Book("testTitle","TestAuthor", "2019",1L);
        testBook.setLend(true);
        assertTrue(testBook.getLend());
    }

    @Test
    public void getSetLendTestFalse() {
        Book testBook = new Book("testTitle","TestAuthor", "2019",1L);
        testBook.setLend(false);
        assertFalse(testBook.getLend());
    }

    @Test
    public void testToStringTestValue() {
        Book testBook = new Book("testTitle","TestAuthor", "2019",1L);
        String expected = "id: " + 1L+ "\n" +
                "title: " + "testTitle" +"\n" +
                "author: " + "TestAuthor" + "\n" +
                "year: " + "2019" + "\n";

        assertEquals(expected, testBook.toString());
    }

    @Test
    public void testToStringTestNull() {
        Book testBook = new Book(null,null, null,null);
        String expected = "id: " + null + "\n" +
                "title: " + null +"\n" +
                "author: " + null + "\n" +
                "year: " + null + "\n";

        assertEquals(expected, testBook.toString());
    }

    @Test
    public void testEqualsHashCodeTestTrue() {
        Book testBook1 = new Book("testTitle","TestAuthor", "2019",1L);
        Book testBook2 = new Book("testTitle","TestAuthor", "2019",2L);
        assertEquals(testBook1, testBook2);
        assertTrue(testBook1.hashCode() == testBook2.hashCode());
    }

    @Test
    public void testEqualsHashCodeTestFalse() {
        Book testBook1 = new Book("testTitle","TestAuthor", "2019",1L);
        Book testBook2 = new Book("testTitle2","TestAuthor", "2019",2L);
        assertNotEquals(testBook1, testBook2);
        assertTrue(testBook1.hashCode() != testBook2.hashCode());
    }

    @Test
    public void testEqualsHashCodeTestTrueNull() {
        Book testBook1 = new Book(null,null, null,null);
        Book testBook2 = new Book(null,null, null,null);
        assertEquals(testBook1, testBook2);
        assertTrue(testBook1.hashCode() == testBook2.hashCode());
    }

    @Test
    public void testEqualsHashCodeTestFalseNull() {
        Book testBook1 = new Book(null,null, null,null);
        Book testBook2 = new Book("testTitle2","TestAuthor", "2019",2L);
        assertNotEquals(testBook1, testBook2);
        assertTrue(testBook1.hashCode() != testBook2.hashCode());
    }


}